\documentclass[12pt, a4paper]{article}
\usepackage[english]{babel}
\usepackage{csquotes}
\usepackage[left=1.5cm, right=1.5cm]{geometry}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{bbm}
\usepackage{tikz}
\usepackage{float}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{pgfplots}
\pgfplotsset{compat=1.9}

\title{Internship Report: EXERSYS - An EXplainable RecommendER SYStem for the Nutrition Domain, combining Knowledge Graphs and Machine Learning}
\author{Ayoub HAMMAL}
\date{}

\graphicspath{{./resources/}}

\begin{document}
    \maketitle
    \tableofcontents

    \section{Introduction} \label{section:introduction}
    
    This internship took place at the \textit{INRAE} laboratory of \textit{Agro Paris Tech}, in the \textit{MIA Paris-Saclay} unit (\textit{Mathématique et Informatique Appliquées de Paris-Saclay}).
    I was under the supervision of:
    \begin{itemize}
        \item Mrs.\ Cristina Manfredotti, Maître de conférences - Ekinocs - AgroParisTech - INRAE - Université Paris-Saclay.
        \item Mr.\ Stéphane Dervaux, Ingénieur en Informatique - Ekinocs - AgroParisTech - INRAE - Université Paris-Saclay.
        \item Mrs.\ Fatiha Saïs, HDR, Professeur - LaHDAK - LISN - Université Paris-Saclay.
        \item Mr.\ Vincent Guigue, HDR, Professeur - Ekinocs - AgroParisTech - INRAE - Université Paris-Saclay.
    \end{itemize}


    \section{Task} \label{section:task}

    My internship was part of the EXERSYS project.
    The goal of the project is to create a food/meal recommendation system using a combination of machine learning techniques and knowledge graphs.
    The recommendation has to take into account consumer's preferences, and different constraints linked to his/her profile.
    The recommendation system uses data collected from a national food consumption survey \textit{INCA 2} \cite{web:fr:inca2}.

    Another M2 intern student is currently tasked with developing a first version of the recommendation system as part of a 6 months internship.
    A brief description of her work will be given in section \ref{section:the_recommendation_system}.
    The project will finance a PhD student who will start in October.

    My main task consisted in collecting required data from heterogeneous sources and creating and populating a knowledge base containing food information and their nutritional composition, as well as information relative to different consumer's profiles such as consumptions, allergies and diets (vegetarian, vegan...).

    Afterward, I implemented the set of constraints extracted from expert's rules which will be used to filter recommendations generated according to the consumer's profile and preferences.

    The PhD student will use the knowledge base generated, the recommendation system implemented as the constraints implemented as a starting point for his/her work.

    \section{Data} \label{section:data}

    The project uses data from the public domain of heterogeneous sources, namely \textit{INCA 2} \cite{web:fr:inca2} and \textit{CIQUAL 2020} \cite{web:fr:ciqual2020}.

    \subsection{INCA 2}

    INCA surveys are national surveys that provides insights on food consumption habits of the population in metropolitain France.
    INCA 2 was produced by ANSES (Agence nationale de sécurité sanitaire, de l'alimentation, de l'environnement et du travail) in 2006-2007 and last updated in 2014 \cite{web:fr:inca2}.

    The dataset is very rich containing different information about the participants' food habits and opinions, and records data about the series of consumption recorded over 7 days in the span of several months to account for weekly and seasonal shifts in consumption habits.
    During this internship, we used only the consumption data.
    The data records weekly consumption of 2624 adults, we discarded information of individuals under 18 years old as they represent a more difficult case of study.

    Consumption records in INCA 2 refer to food and ingredients present in \textit{CIQUAL 2008} which is a food database that has been updated since the time the study was conducted.
    The list of ingredients that compose each food present in INCA 2 is not publicly available, it was acquired through an external contact.
    
    \subsection{CIQUAL 2020}

    ANSES defines the CIQUAL project as:
    \begin{displayquote}
        \guillemotleft The ANSES-CIQUAL food composition table gives information on the average nutritional composition of food consumed in France. \guillemotright
        \begin{flushright}
            ANESE - CIQUAL FAQ
        \end{flushright}
    \end{displayquote}

    The INCA2 survey bases its records on CIQUAL 2008 table, but only CIQUAL 2020 \cite{web:fr:ciqual2020} is now available which we used as a reference to retrieve nutritional composition of the consumed ingredients.
    Although the 2020 table is an updated version of 2008 table, some ingredients have been changed and/or have been updated.
    One important challenge was to assign each INCA2 ingredient to its corresponding entry in the CIQUAL 2020 table.


    \section{The recommendation system} \label{section:the_recommendation_system}

    For the first part of the M2 internship, the recommendation system training data are restricted to breakfast meals only, since lunch and dinner contains richer meals and thus require further processing.

    The first step to build the recommendation system was to model food in a vector space that allows the extraction of similarity scores among individual food items.
    This was performed by training a Word2Vec model of $n$ dimensions on the set of breakfast meals reported in INCA2, where each meal is a set of food items and the context widow was set to the maximum length of a meal.

    Following this step, food were clustered using The KMeans algorithm into different categories.
    The optimal number of categories was defined using expert's knowledge extracted from INCA2 food groups and the silhouette score.

    Then for every consumer $u$ we have the following quantities:
    \begin{itemize}
        \item $f_{c} \in [0, 1]$ their frequency of consumption of cluster $c$: $f_{c} = \frac{|Food_{c} \cap Food_{u}|}{|Food_{u}|}$ where $Food_{c}$ is the set of food items of category $c$ and $Food_{u}$ is the food items consumed by the consumer $u$;
        \item $u_{c} \in \mathbb{R}^{n}$, the average of the food items vectors of cluster $c$ consumed by consumer $u$;
        \item $P_{k|c, u}$, the probability mass function of recommending the food item $k$ from cluster $c$ to the consumer $u$, it is defined as:
            $$ P_{k|c, u} = \frac{e^{\alpha \times \cos (v_{k}, u_{c} \times \mathbbm{1}_{f_{c} > 0} + m_{c} \times \mathbbm{1}_{f_{c} = 0})}}{\sum_{i \in I_{c}} e^{\alpha \times \cos (v_{i}, u_{c} \times \mathbbm{1}_{f_{c} > 0} + m_{c} \times \mathbbm{1}_{f_{c} = 0})}} $$
            where $\alpha \in \mathbb{R}$ is a diversity factor, $\cos$ is a similarity measure, $v_{k} \in \mathbb{R}^{n}$ is the vector representation of food item $k$, $I_{c}$ is the set of food items of cluster $c$ and $m_{c} \in \mathbb{R}^{n}$ is the average of the food items representations of cluster $c$.

    \end{itemize}
    The vector $u_{c} \times \mathbbm{1}_{f_{c} > 0} + m_{c} \times \mathbbm{1}_{f_{c} = 0}$ represents the consumer.
    This formula allows to represent a consumer as the average of their consumptions in a cluster if they already consumed a food item from it ($f_{c} > 0$). Otherwise, a consumer is represented as the average of the food items of the cluster $m_{c}$.

    Currently, the system samples a food recommendation from every cluster from which the consumer already consumed a food item.
    And from clusters that he never consumed with probability $\beta$.

    Sampling only a single food item from each cluster is justified by the statistical observation that consumers take one or less food item from a single cluster in a single meal $98\%$ of the time.

    The sampling processes from each category are independent from each other.
    This is corrected subsequently using expert association rules [sect.\ \ref{section:recommendation_filtering}].


    \section{The knowledge base} \label{section:the_knowledge_base}

    The role of the knowledge base is to formally describe and link the different type of information interacting in the recommendation system.

    The advantage a knowledge base has over a traditional data base is the inference capabilities it has that simplify the data model to a set of logical relations, and which allows us to express constraints in a more declarative fashion (SHACL shapes) rather than programmatically.

    While there are existing knowledge bases that contain food information and recipes such as FooKB \cite{web:en:food_kg}, FoodOn \cite{web:en:foodon} and OpenFoodFacts Ingredients Ontology \cite{web:en:open_food_facts}, none of them contained information about consumption habits or records of meal plans.

    Figure \ref{fig:knowledge_base_schema} summarizes the different OWL classes I defined during this internship represented in the knowledge base as well as their object and data properties.

    The main classes in this knowledge graph are those concerning \textit{Food}, \textit{Ingredients} and \textit{Consumers}.
    More importantly, other information is attached to the ingredients; such as the \textit{allergies} it causes and its \textit{origin}.
    Those will come useful for defining constraints based on allergies and/or vegetarian diets, for example.
    Those information were not present initially in the INCA2 data, they were fetched from other sources such as the OpenFoodFacts Ontology \cite{web:en:open_food_facts}.

    Another important step has been to model consumer's profiles.
    We chose to allow each profile to have its own properties; for instance profiles associated to a certain allergy are linked to the Allergy class, whereas the vegetarian profile is linked to a prohibited food origin.

    Performance of downstream inference tasks as well as further scaling were taken into consideration while planning the knowledge base structure.
    The global knowledge graph is separated into elementary units, with one large graph containing food information (the top portion of the graph in fig.\ \ref{fig:knowledge_base_schema} from the Food class), and a separated graph for each consumer and their respective consumptions.
    While doing inference, only relevant parts of the graph are merged for the purpose of the task at hand.
    This trick reduces not only loading time of the graph (up to 10 times faster), but allows us to diminish SHACL validation requests substantially (up to 100 times faster).


    \section{Recommendation filtering} \label{section:recommendation_filtering}

    For the inference, the system fetches the food graph and the graph of the consumer concerned by the recommendation.
    It inserts the recommendation in the union of those two graphs and validates the resulting graph using a set of SHACL shapes.

    SHACL shapes are a useful formal tool to ensure that a knowledge graph obeys a specific structure.
    It can be applied to both concepts and to properties and generates detailed, customizable messages in case there is any violation.
    This helps improve the explainability of the system's recommendations and gives it more credibility.
    
    As for the constraints, we implemented three main types of constraints: expert's rules, nutriment scores and prohibited food.

    Expert's rules are a set of association, exclusion and cardinality rules.
    They were constructed from statistical observations drawn from the consumption data (e.g.\ butter is always consumed with bread at breakfast).
    
    Nutriment scores and prohibited food rules are profile specific, which means that they depend on the current consumer profile.
    Nutriment scores are optimization constraints, that rank a set of recommendations according to one or more objective functions (e.g.\ an athlete has to maximize protein intake).
    Lastly prohibited food rules are a set of rules that verify if the consumer is allowed to eat the recommended food, this filters out food that causes allergies the consumer suffers from for example.


    \section{Value and Conclusion} \label{section:value_and_conclusion}

    In this internship, I worked on constructing a knowledge base containing data collected from different sources.
    Being interested mainly in knowledge extraction and information retrieval, I learned different useful knowledge representation tools and languages such as RDF/RDF-S \cite{web:en:rdf}, OWL \cite{web:en:owl}, SHACL \cite{web:en:shacl}, SPARQL \cite{web:en:sparql}, description logic \cite{web:en:dl} and more.
    I attended a workshop \footnote{CoCoA BeANS (Cognitive and COmputational Approaches of Behaviour and Nutrition Studies) Workshop, May 11-12 2023.} where I learned about different computational and intelligent methods applied to biology and nutrition problems.
    And more importantly, the final product of my internship will serve as a fundamental basis for subsequent projects and thesis.


    \begin{figure}[H] \centering
        \includegraphics[width=\linewidth, height=550px]{schema_kb.png}
        \caption{The schema of the constructed knowledge base.}
        \label{fig:knowledge_base_schema}
    \end{figure}
    
    \bibliographystyle{IEEEtran}
    \bibliography{refs}

\end{document}
