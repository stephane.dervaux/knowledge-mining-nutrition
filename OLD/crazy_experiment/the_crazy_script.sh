#!/bin/sh

TTY=`tty`

# file to iterate over
source_file=$1
# file to lookup into
lookup_file=$2

# should we override and start from the begining?
if [ -f "matches.csv" ]; then 
    # if the matches file does exit
    echo "override previous matches file? [y/N]"
    read OVERRIDE < $TTY
    if [ "$OVERRIDE" == "Y" ] || [ "$OVERRIDE" == "y" ]; then
        mv matches.csv matches_old.csv
        touch matches.csv
    fi
else
    # otherwise just create an empty file
    touch matches.csv
fi

# number of lines already processed
declare -i n_already_processed=$(wc -l matches.csv | sed 's/\([[:digit:]]\+\) .\+$/\1/')

# total number of lines in source file
declare -i total_lines=$(wc -l $source_file | sed 's/\([[:digit:]]\+\) .\+$/\1/')
# current line counter
declare -i i_line=1

while read line ; do
    if [ $i_line -gt $n_already_processed ]; then
        # find a match
        match=$(cat $lookup_file | fzf -i --prompt="[$i_line / $total_lines] $line > ")

        # store it
        echo -e "$line;$match" >> matches.csv

        # continue?
        echo "continue? [Y/n]"
        read CONTINUE < $TTY
        if [ "$CONTINUE" != "" ] && [ "$CONTINUE" != "Y" ] && [ "$CONTINUE" != "y" ]; then
            exit
        fi
    fi
    let "i_line+=1"
done < "$source_file"
