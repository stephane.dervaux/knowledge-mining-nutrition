from importlib import resources
import json

from rdflib import Graph, Literal, URIRef, Namespace, BNode, Literal, Bag
from rdflib.namespace import RDF, RDFS, SH, XSD, FOAF
from rdflib.container import Container
from rdflib.collection import Collection

from pyshacl import validate

from .metadata import *

class Validator:
    """A validator that checks recommendations against the specified
    knowledge base.

    Attributes
    ----------
    graph : rdflib.Graph
        the knowledge graph against which the recommendations are validated
    local_ns : rdflib.Namespace
        the local namespace
    inca_ns : rdflib.Namespace
        the inca namespace
    ciqual_ns : rdflib.Namespace
        the ciqual namespace

    Methods
    -------
    attach_consumer(self, id_consumer: int) -> None
    insert_recommendation(self, recommendation: dict) -> str
    reset_knowledge_graph(self) -> None
    drop_recommendation(self, recommendation_id: int) -> None
    validate_graph(self, recommendation_id: str, validators: list[dict] = []) -> list[dict]
    validate_allergies(self, recommendation_node: BNode) -> dict
    validate_diet(self, recommendation_node: BNode) -> dict
    validate_association_rules(self, recommendation_node: BNode)
    validate_exclusion_rules(self, recommendation_node: BNode)
    validate_cardinality_rules(self, recommendation_node: BNode)
    """

    def __init__(self):
        # the food knowledge graph 
        food_kb = (
            resources.files("semanticValidator.resources.kb")
            .joinpath("kb_food.ttl")
            .read_text(encoding="utf-8")
        )
        self.food_graph = Graph()
        self.food_graph.parse(data=food_kb)

        self.local_ns = Namespace("http://example.org/1.0/local#")
        self.inca_ns = Namespace("http://example.org/1.0/inca#")
        self.ciqual_ns = Namespace("http://example.org/1.0/ciqual#")

        self.food_graph.bind("local", self.local_ns)
        self.food_graph.bind("inca", self.inca_ns)
        self.food_graph.bind("ciqual", self.ciqual_ns)

        # the graph used for validation
        self.working_graph = None

        # SHACL graph to validate allergies
        self.allergies_shacl_graph = Graph()
        self.allergies_shacl_graph.parse(
            data=resources.files("semanticValidator.resources")
            .joinpath("allergies_shacl_graph.ttl")
            .read_text(encoding="utf-8")
        )

        # SHACL graph to validate vegetarian diet
        self.vegetarian_shacl_graph = Graph()
        self.vegetarian_shacl_graph.parse(
            data=resources.files("semanticValidator.resources")
            .joinpath("vegetarian_shacl_graph.ttl")
            .read_text(encoding="utf-8")
        )

        # SHACL graph to validate association rules
        self.association_rules_shacl_graph = Graph()
        self.association_rules_shacl_graph.parse(
            data=resources.files("semanticValidator.resources")
            .joinpath("association_rules_shacl_graph.ttl")
            .read_text(encoding="utf-8")
        )

        # SHACL graph to validate exclusion rules
        self.exclusion_rules_shacl_graph = Graph()
        self.exclusion_rules_shacl_graph.parse(
            data=resources.files("semanticValidator.resources")
            .joinpath("exclusion_rules_shacl_graph.ttl")
            .read_text(encoding="utf-8")
        )

        # SHACL graph to validate cardinality rules
        self.cardinality_rules_shacl_graph = Graph()
        self.cardinality_rules_shacl_graph.parse(
            data=resources.files("semanticValidator.resources")
            .joinpath("cardinality_rules_shacl_graph.ttl")
            .read_text(encoding="utf-8")
        )

        # SPARQL query to extract result messages
        self.result_messages_sparql_query = (
            resources.files("semanticValidator.resources")
            .joinpath("result_messages_sparql_query.rq")
            .read_text(encoding="utf-8")
        )

        # SPARQL query to extract day of the meals that violate shape
        self.result_meal_day_sparql_query = (
            resources.files("semanticValidator.resources")
            .joinpath("result_meal_day_sparql_query.rq")
            .read_text(encoding="utf-8")
        )

        self.__recommendation_store = dict()

    def attach_consumer(self, id_consumer: int) -> None:
        """Create a working graph that contains the union of the food graph
        and the consumer graph.

        Parameters
        ----------
        id_consumer : int
        """
        # knowledge graph of the consumer and their consumptions
        consumer_kb = (
            resources.files("semanticValidator.resources.kb.consumer_kbs")
            .joinpath(f"{id_consumer}.ttl")
            .read_text(encoding="utf-8")
        )
        consumer_graph = Graph()
        consumer_graph.parse(data=consumer_kb)

        self.working_graph = self.food_graph + consumer_graph

    def insert_recommendation(self, recommendation: dict) -> str:
        """Inserts recommendation in the consumer graph to whom the recommendation
        is made, and changes the working graph to the union of the food graph
        and the new updated consumer graph.

        Parameters
        ----------
        recommendation : dict
            Contains the following keys:
                - "recommendation": list[list[list[int]]] containing the week of
                consumption. Food in the inner most list are referenced using
                their codes.
                - "id_consumer": int
                - "profiles": list[str]

        Returns
        -------
        int
            The id of the inserted recommendation

        Examples
        --------
        >>> validator = Validator()
        >>> recommendation = {
        >>>     "recommendation": [[[18150, 7001, 7620], [25401, 25533], [32018]]],
        >>>     "id_consumer": 110006,
        >>>     "profiles": [
        >>>         "profile-allergique-soja",
        >>>         "profile-allergique-lait",
        >>>         "profile-vegetarien",
        >>>     ],
        >>> }
        >>> recommendation_id = validator.insert_recommendation(recommendation)
        >>> recommendation_id
        'N42c2572c4bd44c3b8199f18b8204ef8f'
        """

        inserted_tuples = []
        
        # inserting the recommendation
        recommendation_node = BNode()
        recommendation_id = str(recommendation_node)
        inserted_tuples.append(
            (recommendation_node, RDF.type, self.local_ns.Recommendation)
        )
        inserted_tuples.append(
            (
                recommendation_node,
                self.local_ns.forConsumer,
                self.local_ns[prefix_consumer + str(recommendation["id_consumer"])],
            )
        )

        for profile in recommendation["profiles"]:
            inserted_tuples.append(
                (
                    self.local_ns[prefix_consumer + str(recommendation["id_consumer"])],
                    self.local_ns.hasConsumerProfile,
                    self.local_ns[profile],
                )
            )

        weekly_consumption = BNode()
        inserted_tuples.append(
            (weekly_consumption, RDF.type, self.local_ns.WeeklyConsumption)
        )
        inserted_tuples.append(
            (recommendation_node, self.local_ns.suggestsConsumption, weekly_consumption)
        )

        for i_day, day in enumerate(recommendation["recommendation"]):
            daily_consumption = BNode()
            inserted_tuples.append(
                (daily_consumption, RDF.type, self.local_ns.DailyConsumption)
            )
            inserted_tuples.append((daily_consumption, self.local_ns.dayOfTheWeek, Literal(i_day + 1, datatype=XSD.integer)))
            inserted_tuples.append(
                (weekly_consumption, self.local_ns.isComposedOf, daily_consumption)
            )

            # there should be 3 meals a day
            for i, recommended_meal in enumerate(day):
                meal = BNode()
                inserted_tuples.append((meal, RDF.type, self.local_ns.Meal))
                inserted_tuples.append(
                    (daily_consumption, self.local_ns.isComposedOf, meal)
                )

                meal_type_label = meal_types[i]

                inserted_tuples.append(
                    (
                        meal,
                        self.local_ns.isOfType,
                        self.local_ns[prefix_meal_type + meal_type_label],
                    )
                )

                for food_code in recommended_meal:
                    food_composition = BNode()
                    inserted_tuples.append(
                        (food_composition, RDF.type, self.local_ns.FoodComposition)
                    )
                    inserted_tuples.append(
                        (meal, self.local_ns.hasFoodComposition, food_composition)
                    )

                    inserted_tuples.append(
                        (
                            food_composition,
                            self.local_ns.hasFood,
                            self.local_ns[prefix_food + str(food_code)],
                        )
                    )

        self.__recommendation_store[recommendation_id] = {
            "recommendation": recommendation,
            "inserted_tuples": inserted_tuples
        }

        for tuple in inserted_tuples:
            self.working_graph.add(tuple)

        return recommendation_id

    def reset_knowledge_graph(self) -> None:
        """Resets the working graph to the food_graph.
        """
        self.working_graph = None

    def drop_recommendation(self, recommendation_id: int) -> None:
        """Removes a recommendation from the working graph.

        Parameters
        ----------
        recommendation_id : int
        """
        tuples = self.__recommendation_store[recommendation_id]["inserted_tuples"]
        for tuple in tuples:
            self.working_graph.remove(tuple)

    def validate_graph(
        self, recommendation_id: str, validators: list[dict] = []
    ) -> list[dict]:
        """Validate a recommendation in the working graph against a set of
        validators, and generates a validation report.

        Parameters
        ----------
        recommendation_id : str
        validators : list[dict]
            A list of validators to verify. Each validator is represented by
            a dictionary containing two keys:
                - "name": str the name of the validator, used to identify its
                result in the validation report.
                - "fn": the method of the validator.

        Returns
        -------
        list[dict]
            The validation report. Each element of the list corrsponds to the
            result of a validator, and has two keys:
                - "name": str the name of the validator.
                - "report": dict
        bool
            Whether or not the recommendation is valid or not.

        Examples
        --------
        >>> validator = Validator()
        >>> recommendation = {
        >>>     "recommendation": [[[18150, 7001, 7620], [25401, 25533], [32018]]],
        >>>     "id_consumer": 110006,
        >>>     "profiles": [
        >>>         "profile-allergique-soja",
        >>>         "profile-allergique-lait",
        >>>         "profile-vegetarien",
        >>>     ],
        >>> }
        >>> recommendation_id = validator.insert_recommendation(recommendation)
        >>> result = validator.validate_graph(
        >>>     recommendation_id=recommendation_id,
        >>>     validators=[
        >>>         {"name": "allergies", "fn": validator.validate_allergies},
        >>>         {"name": "vegetarien", "fn": validator.validate_diet},
        >>>         {"name": "association", "fn": validator.validate_association_rules},
        >>>         {"name": "exclusion", "fn": validator.validate_exclusion_rules},
        >>>         {"name": "cardinality", "fn": validator.validate_cardinality_rules},
        >>>     ],
        >>> )
        """
        output = []
        recommendation_node = BNode(recommendation_id)
        global_conforms = True
        for validator in validators:
            validator_output = {}
            validator_output["name"] = validator["name"]
            validator_output["report"] = validator["fn"](recommendation_node)
            global_conforms = global_conforms and validator_output["report"]["conforms"]
            output.append(validator_output)
        return output, global_conforms

    def validate_allergies(self, recommendation_node: BNode) -> dict:
        """Checks if the recommendation's food contains any ingredient that
        causes one of the consumers allergies.
        """
        return self.__validate_ingredients(recommendation_node, self.allergies_shacl_graph)

    def validate_diet(self, recommendation_node: BNode) -> dict:
        """Checks if the recommendation contains food of an origin that the
        consumer does not eat.
        """
        return self.__validate_ingredients(recommendation_node, self.vegetarian_shacl_graph)

    def validate_association_rules(self, recommendation_node: BNode):
        """Checks the expert association rules on the breakfast meal of the
        recommendation.
        """
        return self.__validate_expert_rules(recommendation_node, self.association_rules_shacl_graph)

    def validate_exclusion_rules(self, recommendation_node: BNode):
        """Checks the expert exclusion rules on the breakfast meal of the
        recommendation.
        """
        return self.__validate_expert_rules(recommendation_node, self.exclusion_rules_shacl_graph)

    def validate_cardinality_rules(self, recommendation_node: BNode):
        """Checks the expert cardinality rules on the breakfast meal of the
        recommendation.
        """
        return self.__validate_expert_rules(recommendation_node, self.cardinality_rules_shacl_graph)

    def __validate_ingredients(self, recommendation_node: BNode, shacl_graph: Graph):
        """
        """
        recommendation_id = str(recommendation_node)
        shacl_graph.add(
            (self.local_ns.RecommendationShape, SH.targetNode, recommendation_node)
        )

        conforms, results_graph, results_text = validate(
            self.working_graph, shacl_graph=shacl_graph
        )

        if conforms:
            output = {"conforms": True, "messages": []}
        else:
            output = {"conforms": False, "messages": []}
            for row in results_graph.query(self.result_messages_sparql_query):
                output["messages"].append(json.loads(row.message))
            output["filter"] = self.__create_filter(self.__recommendation_store[recommendation_id]["recommendation"]["recommendation"], output["messages"])

        shacl_graph.remove(
            (self.local_ns.RecommendationShape, SH.targetNode, recommendation_node)
        )

        return output

    @staticmethod
    def __create_filter(recommendation: list[list[list]], messages: list[dict]):
        non_conforming_food = set([int(message["food"]["code"]) for message in messages])

        output = []
        for day in recommendation:
            output_day = []
            for meal in day:
                output_meal = []
                for food in meal:
                    if food in non_conforming_food:
                        output_meal.append(False)
                    else:
                        output_meal.append(True)
                output_day.append(output_meal)
            output.append(output_day)
        return output
    
    def __validate_expert_rules(self, recommendation_node: BNode, shacl_graph: Graph):
        """
        """
        recommendation_id = str(recommendation_node)
        shacl_graph.add(
            (self.local_ns.RecommendationShape, SH.targetNode, recommendation_node)
        )
        
        conforms, results_graph, results_text = validate(
            self.working_graph, shacl_graph=shacl_graph
        )

        if conforms:
            output = {"conforms": True, "filter": []}
        else:
            output = {"conforms": False, "filter": [True] * len(self.__recommendation_store[recommendation_id]["recommendation"]["recommendation"])}
            for row in results_graph.query(self.result_meal_day_sparql_query):
                output["filter"][int(row.day) - 1] = False

        shacl_graph.remove(
            (self.local_ns.RecommendationShape, SH.targetNode, recommendation_node)
        )

        return output
