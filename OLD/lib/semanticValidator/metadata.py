prefix_food_group = "food_group-"
prefix_food_subgroup = "food_subgroup-"
prefix_food = "food-"
prefix_food_category = "food_category-"
prefix_ingredient = "ingredient-"
prefix_ingredient_group = "ingredient_group-"
prefix_ingredient_subgroup = "ingredient_subgroup-"
prefix_ingredient_subsubgroup = "ingredient_subsubgroup-"
prefix_nutriment = "nutriment-"
prefix_unit_of_measure = "unit-"
prefix_allergy = "allergy-"
prefix_origin = "origin-"
prefix_sex = "sex-"
prefix_consumer = "consumer-"
prefix_meal_type = "meal_type-"
prefix_profile = "profile-"
prefix_recommendation = "recommendation-"

meal_types = ["petit-dejeuner", "dejeuner", "diner"]

profiles = [
    "profile-allergique-anhydride_sulfureux_et_sulfites",
    "profile-allergique-arachides",
    "profile-allergique-crustacés",
    "profile-allergique-céleri",
    "profile-allergique-fruits_à_coque",
    "profile-allergique-gluten",
    "profile-allergique-graines_de_sésame",
    "profile-allergique-lait",
    "profile-allergique-lupin",
    "profile-allergique-mollusques",
    "profile-allergique-moutarde",
    "profile-allergique-poisson",
    "profile-allergique-soja",
    "profile-allergique-œufs",
    "profile-lacto-vegetarien",
    "profile-ovo-lacto-vegetarien",
    "profile-ovo-vegetarien",
    "profile-vegetalien",
]

# liste des nutriments ciqual {code: libellé}
nutriments = {
    "energie_kj": "Energie, Règlement UE N° 1169/2011 (kJ/100 g)",
    "energie_kcal": "Energie, Règlement UE N° 1169/2011 (kcal/100 g)",
    "energie_jones_fibre_kj": "Energie, N x facteur Jones, avec fibres  (kJ/100 g)",
    "energie_jones_fibre_kcal": "Energie, N x facteur Jones, avec fibres  (kcal/100 g)",
    "eau_g": "Eau (g/100 g)",
    "proteines_jones_g": "Protéines, N x facteur de Jones (g/100 g)",
    "proteines_6_25_g": "Protéines, N x 6.25 (g/100 g)",
    "glucides_g": "Glucides (g/100 g)",
    "lipides_g": "Lipides (g/100 g)",
    "sucres_g": "Sucres (g/100 g)",
    "fructose_g": "Fructose (g/100 g)",
    "galactose_g": "Galactose (g/100 g)",
    "glucose_g": "Glucose (g/100 g)",
    "lactose_g": "Lactose (g/100 g)",
    "maltose_g": "Maltose (g/100 g)",
    "saccharose_g": "Saccharose (g/100 g)",
    "amidon_g": "Amidon (g/100 g)",
    "fibres_alimentaires_g": "Fibres alimentaires (g/100 g)",
    "plyols_totaux_g": "Polyols totaux (g/100 g)",
    "cendres_g": "Cendres (g/100 g)",
    "alcool_g": "Alcool (g/100 g)",
    "acides_organiques_g": "Acides organiques (g/100 g)",
    "ag_satures_g": "AG saturés (g/100 g)",
    "ag_monoinsatures_g": "AG monoinsaturés (g/100 g)",
    "ag_polyinsatures_g": "AG polyinsaturés (g/100 g)",
    "ag_4_0_g": "AG 4:0, butyrique (g/100 g)",
    "ag_6_0_g": "AG 6:0, caproïque (g/100 g)",
    "ag_8_0_g": "AG 8:0, caprylique (g/100 g)",
    "ag_10_0_g": "AG 10:0, caprique (g/100 g)",
    "ag_12_0_g": "AG 12:0, laurique (g/100 g)",
    "ag_14_0_g": "AG 14:0, myristique (g/100 g)",
    "ag_16_0_g": "AG 16:0, palmitique (g/100 g)",
    "ag_18_0_g": "AG 18:0, stéarique (g/100 g)",
    "ag_18_1_g": "AG 18:1 9c (n-9), oléique (g/100 g)",
    "ag_18_2_g": "AG 18:2 9c,12c (n-6), linoléique (g/100 g)",
    "ag_18_3_g": "AG 18:3 c9,c12,c15 (n-3), alpha-linolénique (g/100 g)",
    "ag_20_4_g": "AG 20:4 5c,8c,11c,14c (n-6), arachidonique (g/100 g)",
    "ag_20_5_g": "AG 20:5 5c,8c,11c,14c,17c (n-3) EPA (g/100 g)",
    "ag_22_6_g": "AG 22:6 4c,7c,10c,13c,16c,19c (n-3) DHA (g/100 g)",
    "cholesterol_mg": "Cholestérol (mg/100 g)",
    "sel_chlorure_sodium_g": "Sel chlorure de sodium (g/100 g)",
    "calcium_mg": "Calcium (mg/100 g)",
    "chlorure_mg": "Chlorure (mg/100 g)",
    "cuivre_mg": "Cuivre (mg/100 g)",
    "fer_mg": "Fer (mg/100 g)",
    "iode_µg": "Iode (µg/100 g)",
    "magnesium_mg": "Magnésium (mg/100 g)",
    "manganese_mg": "Manganèse (mg/100 g)",
    "phosphore_mg": "Phosphore (mg/100 g)",
    "potasium_mg": "Potassium (mg/100 g)",
    "selenium_µg": "Sélénium (µg/100 g)",
    "sodium_mg": "Sodium (mg/100 g)",
    "zinc_mg": "Zinc (mg/100 g)",
    "retinol_µg": "Rétinol (µg/100 g)",
    "beta-carotene_µg": "Beta-Carotène (µg/100 g)",
    "vitamine_d_µg": "Vitamine D (µg/100 g)",
    "vitamine_e_mg": "Vitamine E (mg/100 g)",
    "vitamine_k1_µg": "Vitamine K1 (µg/100 g)",
    "vitamine_k2_µg": "Vitamine K2 (µg/100 g)",
    "vitamine_c_mg": "Vitamine C (mg/100 g)",
    "vitamine_b1_mg": "Vitamine B1 ou Thiamine (mg/100 g)",
    "vitamine_b2_mg": "Vitamine B2 ou Riboflavine (mg/100 g)",
    "vitamine_b3_mg": "Vitamine B3 ou PP ou Niacine (mg/100 g)",
    "vitamine_b5_mg": "Vitamine B5 ou Acide pantothénique (mg/100 g)",
    "vitamine_b6_mg": "Vitamine B6 (mg/100 g)",
    "vitamine_b9_µg": "Vitamine B9 ou Folates totaux (µg/100 g)",
    "vitamine_b12_µg": "Vitamine B12 (µg/100 g)",
}
