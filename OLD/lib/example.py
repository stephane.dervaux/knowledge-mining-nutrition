import time
from pprint import pprint

from semanticValidator import Validator
from semanticValidator.metadata import *

# to be replaced by the path to the knowledge base file
# AU TOUT DEBUT
start_time = time.time()
print("loading the knowledge base... ", end="")
validator = Validator()
print(f"done! {time.time() - start_time} s")

id_consumer = 110006

start_time = time.time()
print("adding the consumer... ", end="")
validator.attach_consumer(id_consumer)
print(f"done! {time.time() - start_time} s")
################################################################
# RECOMMANDAITON 1
recommendation = {
    "recommendation": [
        [
            [18150, 7001, 7620, 24630], 
            [25401, 25533], 
            [32018]
        ]
    ],  # jours  # repas
    "id_consumer": id_consumer,
    "profiles": [
        "profile-allergique-soja",
        "profile-allergique-lait",
        "profile-allergique-œufs",
        "profile-ovo-lacto-vegetarien",
    ],
}

start_time = time.time()
print("inserting the recommendation... ", end="")
recommendation_id = validator.insert_recommendation(recommendation)
print(f"done! {time.time() - start_time} s")

start_time = time.time()
print("validating the recommendation... ", end="")
result = validator.validate_graph(
    recommendation_id=recommendation_id,
    validators=[
        {"name": "allergies", "fn": validator.validate_allergies},
        {"name": "vegetarien", "fn": validator.validate_diet},
        {"name": "association", "fn": validator.validate_association_rules},
        {"name": "exclusion", "fn": validator.validate_exclusion_rules},
        {"name": "cardinality", "fn": validator.validate_cardinality_rules},
    ],
)
print(f"done! {time.time() - start_time} s")

pprint(result)

start_time = time.time()
print("dropping the recommendation... ", end="")
validator.drop_recommendation(recommendation_id)
print(f"done! {time.time() - start_time} s")

## RECOMMANDATION 2 (la meme)
start_time = time.time()
print("inserting the recommendation... ", end="")
recommendation_id = validator.insert_recommendation(recommendation)
print(f"done! {time.time() - start_time} s")

start_time = time.time()
print("validating the recommendation... ", end="")
result = validator.validate_graph(
    recommendation_id=recommendation_id,
    validators=[
        {"name": "allergies", "fn": validator.validate_allergies},
        {"name": "vegetarien", "fn": validator.validate_diet},
        {"name": "association", "fn": validator.validate_association_rules},
        {"name": "exclusion", "fn": validator.validate_exclusion_rules},
        {"name": "cardinality", "fn": validator.validate_cardinality_rules},
    ],
)
print(f"done! {time.time() - start_time} s")

pprint(result)

start_time = time.time()
print("dropping the recommendation... ", end="")
validator.drop_recommendation(recommendation_id)
print(f"done! {time.time() - start_time} s")


# A LA TOUTE FIN
start_time = time.time()
print("reseting the knowledge graph... ", end="")
validator.reset_knowledge_graph()
print(f"done! {time.time() - start_time} s")
######################################################