from semanticValidator import Validator
from semanticValidator.metadata import *

from rdflib import Graph, Literal, URIRef, Namespace, BNode, Literal, Bag
from rdflib.namespace import RDF, RDFS, SH, XSD, FOAF
from rdflib.container import Container
from rdflib.collection import Collection
from rdflib.extras.external_graph_libs import rdflib_to_networkx_multidigraph

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from tqdm import tqdm, trange

import time
import pprint

# to be replaced by the path to the knowledge base file
# AU TOUT DEBUT
start_time = time.time()
print("loading the knowledge base... ", end="")
validator = Validator()
print(f"done! {time.time() - start_time} s")

id_consumer = 110006

start_time = time.time()
print("adding the consumer... ", end="")
validator.attach_consumer(id_consumer)
print(f"done! {time.time() - start_time} s")
################################################################

query_food_per_cat="""
SELECT ?foodCode
WHERE {{
    ?food a local:Food .
    ?food local:code ?foodCode .
    ?food local:hasCategory ?category .
    ?category local:code "{}"^^xsd:integer
}}"""

nb_categories = 8

ratio_per_cat_prof = np.zeros((nb_categories, len(profiles)), dtype=float)

validator_per_profile = {
    "profile-allergique-anhydride_sulfureux_et_sulfites": validator.validate_allergies,
    "profile-allergique-arachides": validator.validate_allergies,
    "profile-allergique-crustacés": validator.validate_allergies,
    "profile-allergique-céleri": validator.validate_allergies,
    "profile-allergique-fruits_à_coque": validator.validate_allergies,
    "profile-allergique-gluten": validator.validate_allergies,
    "profile-allergique-graines_de_sésame": validator.validate_allergies,
    "profile-allergique-lait": validator.validate_allergies,
    "profile-allergique-lupin": validator.validate_allergies,
    "profile-allergique-mollusques": validator.validate_allergies,
    "profile-allergique-moutarde": validator.validate_allergies,
    "profile-allergique-poisson": validator.validate_allergies,
    "profile-allergique-soja": validator.validate_allergies,
    "profile-allergique-œufs": validator.validate_allergies,
    "profile-lacto-vegetarien": validator.validate_diet,
    "profile-ovo-lacto-vegetarien": validator.validate_diet,
    "profile-ovo-vegetarien": validator.validate_diet,
    "profile-vegetalien": validator.validate_diet,
}

for i_category in trange(nb_categories):
    for i_profile, profile in enumerate(profiles):
        cpt_valid = 0
        cpt_total = 0
        for row in validator.working_graph.query(query_food_per_cat.format(i_category)):
            recommendation = {
                "recommendation": [
                    [   
                        [int(row.foodCode)], 
                    ]
                ],  # jours  # repas
                "id_consumer": id_consumer,
                "profiles": [
                    profile
                ],
            }
            recommendation_id = validator.insert_recommendation(recommendation)

            _, result = validator.validate_graph(
                recommendation_id=recommendation_id,
                validators=[
                    {"name": "validator_of_profile", "fn": validator_per_profile[profile]},
                ],
            )
            if result:
                cpt_valid += 1
            cpt_total += 1

            validator.drop_recommendation(recommendation_id)
            
        ratio_per_cat_prof[i_category, i_profile] = cpt_valid / cpt_total

################################################################

# A LA TOUTE FIN
start_time = time.time()
print("reseting the knowledge graph... ", end="")
validator.reset_knowledge_graph()
print(f"done! {time.time() - start_time} s")
######################################################

sns.heatmap(ratio_per_cat_prof, xticklabels=profiles, yticklabels=list(range(nb_categories)))
plt.show()