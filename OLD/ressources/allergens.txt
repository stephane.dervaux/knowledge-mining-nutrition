fr:gluten, blé, seigle, orge, épeautre, kamut, son de blé, blé complet, gluten de blé, fibre de blé, malt d'orge, malt, froment, farine de blé, farine de froment, boulgour, petit épeautre, céréales contenant du gluten, blé dur, épeautre, petit épeautre, grand épeautre, farine d'épeautre

fr:crustacés, crustacé, crabe, crabes, homard, homards, langouste, langoustine, gambas, crevettes, crevette, écrevisses, écrevisse

fr:œufs, œuf, oeufs, oeuf, blanc d'oeuf, jaune d'oeuf, blanc d'œuf, jaune d'œuf, blanc d'oeufs, jaune d'oeufs, blanc d'œufs, jaune d'œufs, oeuf frais, œuf frais, œufs frais, oeufs frais

fr:poisson, poissons, cabillaud, morue, maquereau, flétan, turbot, colin, haddock, aiglefin, saumon, sole, truite, thon, sardine, sardines, loup, colin d'Alaska, anchois, brochet, hareng, harengs, merlu, lieu, merlan, saumon fumé, rouget, limande, tacaud

fr:arachides, arachide, cacahuètes, cacahuète, cacahouètes, cacahouète, arachis hypogaea

fr:soja, soya, soia, sojas, son de soja, lécithine de soja, tonyu, graine de soja, graines de soja, tofu

fr:lait, lactose, laitier, laitiere, laitiers, crème, beurre, bas-beurre, babeurre, beurre patissier, petit-lait, yaourt, fromage, méton, lait cru, Emmental, comte, bleu, edam, lactique, lactiques, ferments lactiques, cheddar, reblochon, parmesan, mimolette, crème de lait, fromage de vache, fromage de chèvre, gouda, lait écrémé, lait entier, lactoserum, lactosérum, mozzarella, protéines de lait, crème fraiche, ricotta, fromage blanc, mascarpone, caséinate, caséine, poudre de lait, Produits laitiers et dérivées, Produits laitiers et dérivés, Produits laitiers, Gorgonzola, Roquefort, Fromage fondu, Fromage frais, Feta, Raclette, laits et dérivés y compris lactose, lait de vache, lait de chèvre, lait de brebis, dérivés laitiers, protéines laitières, beurre concentré, gruyère, lait demi-écrémé, lait écrémé, lait frais

fr:fruits à coque, amandes, noisettes, châtaigne, noix, noix de cajou, noix de pécan, noix du Brésil, pistaches, noix de Macadamia, noix du Queensland, fruits secs, autres fruits secs, autres fruits à coque, fruits secs à coque, fruits à coque dure

fr:céleri, céleri-rave

fr:moutarde, graines de moutarde

fr:graines de sésame, graine de sésame, sésame

fr:anhydride sulfureux et sulfites, anhydride sulfureux, sulfites, sulfite, sulphites, sulphite, bisulfite, metabisulfite, metabisulphite, disulfites

fr:lupin

fr:mollusques, mollusque, poulpe, seiche, encornet, calamar, calmar, huitres, huitre, moules, moule, noix de saint-jacques, noix de st-jacques, noix de coquilles saint-jacques, noix de coquilles st-jacques, escargot, escargots, saint-jacques, st-jacques, coquille saint jacques, coquille st jacques, pétoncles