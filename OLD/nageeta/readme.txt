**Folder: `kb_inca2_inca3\consumer_kbs` contains the knowledge base , and the file name is the unique identifier for each consumer_kbs

**Folder: `mapping_foodex2` contains the script file 'foodex2_inca2_base_term_mapping_sca.ipynb' which has the script to map inca2 with foodex, 
--> to run that script file, start the server which is in folder 'foodex2-sca-backend' and to run that simply create a virtual and install packages available on 'requiremnets.txt' file
->after strating server you can run script inside 'foodex2_inca2_base_term_mapping_sca.ipynb'  file and you will get the results in a csv file


**Folder: `kb_inca2_inca3` conatins some experiments that we did during the mapping of food for foodex code from inca2 to inca3 

**Folder: `kb/consumer_kbs` is the old knowledge base which has data for inca2 and inca3 only for consumer part

**File: `deepl-translation.ipynb`: has the script that uses deepl api to translate the inca2 food name, group name and sub group name to english

**File: `knowledge_construction.ipynb` has the code for the construction of knowledge base for both inca2 and inca3 data

**File: `ontology.ttl` is the ttl file of the final ontology

**File: `ontology.drawio.xml` is the xml file of the ontology

Other files has some exploration of data and some experiments