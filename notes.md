- aliments
    - codgr : code groupe
    - libgr : libellé groupe
    - codal : code aliment
    - libal : libellé aliment

- ingrédients
    - iding : identifiant ingrédient
    - libing : libellé ingrédient
    - typing : "brut ou peu transformé"/"transformé"
    - source : 

- charactéristiques
    - taxonomie (origine)
    - allergène

anca 3 / foodex 3

- taxonomie ingrédients/aliments
- profils utilisateur : differents profils possible qui viennent avec leurs interdictions
- consommations de l'utilisateur
- règles d'association et d'exclusion, règles nutritionnelles

- entrée : séquence de repas pour une semaine
- sortie : filtrage
- TODO définir les messages échangés avec le système de recommendation
    - entrée : suggestion des repas liste de liste d'entiers + profil utilisateur + préférences nutritionnelles (JSON) 
    - sortie : même liste de liste de booléens (accepté/refusé) (JSON)

ref ciqual : que 2020, base de données différentes des ingrédients de ANCA2 basés sur la base données 2008

- informations sur les ingrédients :
    - origine : végétale ou animale (peut-être ça serait mieux de faire cette classification au niveau des aliments) - à faire
    - composition nutritionnelle : pour chaque ingrédient - DONE
        - récupérer les 5 meilleurs matches de CIQUAL 2020 depuis leur site web
        - filtrer à la main ces matches
        - pour les ingrédients non retrouvés dans CIQUAL 2020, récupérer la composition nutritionnelle ailleurs
    - allergène (depuis : https://github.com/openfoodfacts/openfoodfacts-server/blob/main/taxonomies/allergens.txt, 
                          https://www.economie.gouv.fr/dgccrf/Publications/Vie-pratique/Fiches-pratiques/Allergene-alimentaire) - DONE
        - tester si un des allergènes est présent dans le nom de l'ingrédient, si c'est le cas, ajouter l'allergie à sa liste d'allergies

- classes :
    - groupe aliments - DONE
        - code
        - nom
    - aliment - DONE
        - code
        - nom
    - ingrédient - DONE
        - code
        - nom
        - dérivé de (origine)
        - contient (nutriment)
        - cause (allergie)
    - nutriment - DONE
        - code
        - nom
    - origine
        - chair animale
        - lait et dérivés
        - oeufs et dérivés
        - miel et dérivés
        - champignons, levures et dérivés
        - végéteaux et dérivés
    - allergie - DONE
        - code
        - nom
    
    - utilisateur - DONE
        - code
        - consommation? : séquences (semaine) séquences (jour) de repas
    - repas : sac d'aliment - DONE
    - petit déjeuner - DONE
    - déjeuner - DONE
    - dîner - DONE
    
    - profils
        - végétarien (https://fr.wikipedia.org/wiki/V%C3%A9g%C3%A9tarisme#Tableau_synoptique_des_diff%C3%A9rentes_pratiques_v%C3%A9g%C3%A9tariennes)
            - liste d'origines non autorisées
        - allergique
            - allergie

    - différents profils possible pour un utilisateur : végétarien/végétalien, allergique à, besoins nutritionnels - DONE
        - à un utilisateur, on associe un sac de profils
        - végétarie
        - allergique

IDEE : tirer plusieurs recommandations et les trier selon leurs qualités (fonction d'apppréciation à définir)

---
```
dish is composed of food
food is composend of ingredientComposition
food has category anca:food category

ingredientComposition hasIng ingredient
ingredientComposition hasQte qte

ingredient name lit
ingredient code lit
ingredient hasCiqualCategory ciqualCategory
ciqualCategory rdfs:label lit
ciqualCategory causes allergy
ingredient causes allergy
ingredient hasNutComp NutComp

NutComp hasQte lit
NutComp hasNutriment nutriment

allergy rdfs:label lit

nutriment code
        nom
        hasMU MU

consommateur hasConsommation consommationHebdomadaire
consommateur hasAge
            hasGender
            hasProfile profile

profile:
    - ensemble de'aliment à exclure
    - à définir 

RulesToApply en relation avec des profils
            
time ontology pour les consommations => repas sans timestamp

consommationHebdomadaire isComposedOf consommationJournalière

consommationJournalière isComposedOf* meal
consommationJournalière jourSemaine lit(lundi, mardi, ...)
meal isComposedOf* dish (peut-être catégorisé en entrée/plât/..)
meal isTypeOf mealType
meal isRecommendedFor consommateur
mealType (dej, petit-dej, diner)
meal hasConsommationContexte consommationContexte
consommationContexte (home, outdoor, ..)
```

<img src="OLD/ressources/schema_kb.png">

---

### Les classes et propriétés
```
- local:Dish ???
    - local:isComposedOf*              local:Food
    
- local:Food - DONE
    - rdfs:label                       rdf:langString
    - local:code                       xsd:integer
    - local:hasIncaGroup               inca:FoodGroup
    - local:hasIncaSubGroup            inca:FoodSubGroup
    - local:hasIngredientComposition*  local:IngredientComposition
    - local:hasCategory*  ???          local:FoodGroup

- inca:FoodSubGroup - DONE
    - rdfs:label                       rdf:langString
    - local:code                       xsd:integer

- inca:FoodGroup - DONE
    - rdfs:label                       rdf:langString
    - local:code                       xsd:integer

- local:FoodCategory
    - local:code                       xsd:integer
    
- local:IngredientComposition - DONE
    - local:hasIngredient              local:Ingredient
    - local:hasQuantity                xsd:decimal

- local:Ingredient - DONE
    - rdfs:label                       rdf:langString
    - local:code                       xsd:integer
    - local:hasCiqualGroup             ciqual:IngredientGroup
    - local:hasCiqualSubGroup          ciqual:IngredientSubGroup
    - local:hasCiqualSubSubGroup       ciqual:IngredientSubSubGroup
    - local:hasNutrimentComposition*   local:NutrimentComposition
    - local:causes*                    local:Allergy
    - local:isDerivedFrom*             local:Origin

- local:Origin - DONE
    - rdfs:label                       rdf:langString
    - local:code                       xsd:string

- ciqual:IngredientGroup - DONE
    - rdfs:label                       rdf:langString
    - local:code                       xsd:integer

- ciqual:IngredientSubGroup - DONE
    - rdfs:label                       rdf:langString
    - local:code                       xsd:integer
    - local:isSubGroupOf               local:IngredientGroup  ???

- ciqual:IngredientSubSubGroup - DONE
    - rdfs:label                       rdf:langString
    - local:code                       xsd:integer
    - local:isSubGroupOf               local:IngredientSubGroup  ???

- local:NutrimentComposition - DONE
    - local:hasNutriment               local:Nutriment
    - local:hasQuantity                xsd:decimal

- local:Nutriment - DONE
    - rdfs:label                       rdf:langString
    - local:code                       xsd:integer
    - loca:hasUnitOfMeasure            local:UnitOfMeasure

- local:UnitOfMeasure - DONE
    - rdfs:label                       xsd:string
    - local:code                       xsd:string

- local:Allergy - DONE
    - rdfs:label                       rdf:langString
    - local:code                       xsd:string

- local:Sex - DONE
    - rdfs:label                       rdf:langString

- local:Consumer (rdfs:subClassOf      foaf:Person)?  - DONE
    - local:hasAge                     xsd:integer
    - local:hasSex                     local:Sex    
    - local:hasConsumption*            local:WeeklyConsumption
    - local:hasConsumerProfile*        local:Profile
    
- local:ConsumerProfile  ???

- local:WeeklyConsumption - DONE
    - local:isComposedOf*              local:DailyConsumption

- local:DailyConsumption - DONE
    - local:dayOfTheWeek               xsd:integer
    - local:isComposedOf*              local:Meal

- local:Meal - DONE
    - local:hasFoodComposition*        local:FoodComposition ????
    - local:hasConsumptionContext      local:ConsumptionContext
    - local:isOfType                   local:MealType

- local:FoodComposition - DONE
    - local:hasFood                    local:Food
    - local:hasNetQuantity             xsd:decimal
    - local:hasGrossQuantity           xsd:decimal

- local:MealType - DONE
    - rdfs:label                       rdf:langString

- local:ConsumptionContext ???
    - rdfs:label                       rdf:langString
```

- foodex2
- chowlk
- passer les recommandations sur le graphe

GEMRCN
https://www.economie.gouv.fr/daj/recommandation-nutrition

- inca3

- GraphDB
- séparer les graphes de chaque utilisateur
- optimiser la requête shacl

- noémie: intro
- ontology
- suite
- contraintes
- utilisation de l'outil

- 25 min de présentation

- clustering guidé par la sémantique

- alignement des ontologies

- oaei 
- log map
- 