@prefix foodex: <https://data.food.gov.uk/codes/foodtype/id#> .
@prefix kmn: <http://opendata.inrae.fr/kmn/> .
@prefix time: <http://www.w3.org/2006/time#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

kmn:consumer-INCA3-211100201 a kmn:Female ;
    kmn:belongsTo "INCA3"^^xsd:string ;
    kmn:code 211100201 ;
    kmn:hasAgeGroup kmn:Age45To64 ;
    kmn:hasConsumption kmn:consumption-1_consumer-INCA3-211100201,
        kmn:consumption-2_consumer-INCA3-211100201,
        kmn:consumption-3_consumer-INCA3-211100201 ;
    kmn:hasMedicalInformation [ a kmn:MedicalInformation ;
            kmn:hasBmi "26.09"^^xsd:float ;
            kmn:hasHeight "174.0"^^xsd:float ;
            kmn:hasWeight "79.0"^^xsd:float ] ;
    kmn:hasPersonalProfile kmn:LooseWeightDiet ;
    kmn:isCurrentlyPregnant false ;
    kmn:isMenstrualCycleStops false .

kmn:consumption-1_consumer-INCA3-211100201 a kmn:FullDayConsumption ;
    kmn:consumptionNumber 1 ;
    kmn:isComposedOf kmn:meal-Dinner_consumption-1_consumer-INCA3-211100201 ;
    time:before kmn:consumption-2_consumer-INCA3-211100201 .

kmn:meal-Breakfast_consumption-2_consumer-INCA3-211100201 a kmn:Meal ;
    kmn:hasFoodComposition [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A03KB#F03.A06JL$F22.A07SH"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F22.A07SH"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F03.A06JL"^^xsd:string ] ;
                    kmn:label "café court avec caféine (classique)"^^xsd:string ;
                    kmn:originalCode "1478"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A03KB"^^xsd:string ] ] ;
            kmn:quantity 70.0 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A046P#F03.A06JH"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F03.A06JH"^^xsd:string ] ;
                    kmn:label "aspartame"^^xsd:string ;
                    kmn:originalCode "1851"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A046P"^^xsd:string ] ] ;
            kmn:quantity 0.059999999 ] ;
    kmn:mealType "Breakfast"^^xsd:string ;
    time:before kmn:meal-Lunch_consumption-2_consumer-INCA3-211100201 .

kmn:meal-Breakfast_consumption-3_consumer-INCA3-211100201 a kmn:Meal ;
    kmn:hasFoodComposition [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A046P#F03.A06JH"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F03.A06JH"^^xsd:string ] ;
                    kmn:label "aspartame"^^xsd:string ;
                    kmn:originalCode "1851"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A046P"^^xsd:string ] ] ;
            kmn:quantity 0.12 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A03KB#F22.A07SH$F28.A07MR"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F22.A07SH"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F28.A07MR"^^xsd:string ] ;
                    kmn:label "café court n.s."^^xsd:string ;
                    kmn:originalCode "1486"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A03KB"^^xsd:string ] ] ;
            kmn:quantity 90.0 ] ;
    kmn:mealType "Breakfast"^^xsd:string ;
    time:before kmn:meal-Lunch_consumption-3_consumer-INCA3-211100201 .

kmn:meal-Dinner_consumption-1_consumer-INCA3-211100201 a kmn:Meal ;
    kmn:hasFoodComposition [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A044G#F04.A015S$F19.A07PF"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F04.A015S"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F19.A07PF"^^xsd:string ] ;
                    kmn:label "moutarde à l'ancienne"^^xsd:string ;
                    kmn:originalCode "3430"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A044G"^^xsd:string ] ] ;
            kmn:quantity 5.0999999 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A03XG#F04.A01RG$F04.A01QV$F17.A07MX$F17.A07NA$F22.A07SS$F28.A07GZ$F28.A07KP"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F28.A07GZ"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F04.A01RG"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F17.A07MX"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F04.A01QV"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F22.A07SS"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F17.A07NA"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F28.A07KP"^^xsd:string ] ;
                    kmn:label "boulette de viande"^^xsd:string ;
                    kmn:originalCode "1906"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A03XG"^^xsd:string ] ] ;
            kmn:quantity 40.0 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A03DR#F19.A07PR"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F19.A07PR"^^xsd:string ] ;
                    kmn:label "eau minérale plate non aromatisée"^^xsd:string ;
                    kmn:originalCode "1502"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A03DR"^^xsd:string ] ] ;
            kmn:quantity 500.0 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A01SP#F02.A06AG$F17.A07MV$F17.A07MZ$F20.A07QQ$F28.A07GZ"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F17.A07MZ"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F20.A07QQ"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F17.A07MV"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F02.A06AG"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F28.A07GZ"^^xsd:string ] ;
                    kmn:label "aile de poulet"^^xsd:string ;
                    kmn:originalCode "843"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A01SP"^^xsd:string ] ] ;
            kmn:quantity 41.292 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A045K#F04.A044L"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F04.A044L"^^xsd:string ] ;
                    kmn:label "vinaigrette à base de vinaigre n.s."^^xsd:string ;
                    kmn:originalCode "3796"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A045K"^^xsd:string ] ] ;
            kmn:quantity 4.2800002 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A042C"^^xsd:string ;
                    kmn:label "salade verte mêlée"^^xsd:string ;
                    kmn:originalCode "3779"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A042C"^^xsd:string ] ] ;
            kmn:quantity 30.0 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A045A#F04.A037D$F04.A02NC$F22.A07SS"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F04.A02NC"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F22.A07SS"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F04.A037D"^^xsd:string ] ;
                    kmn:label "aïoli (mayonnaise, ail)"^^xsd:string ;
                    kmn:originalCode "1755"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A045A"^^xsd:string ] ] ;
            kmn:quantity 7.6799998 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A01RG#F17.A07MX$F17.A07MZ$F20.A0F4V$F26.A07XE$F28.A07GZ$F28.A07KP"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F28.A07GZ"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F28.A07KP"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F17.A07MZ"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F26.A07XE"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F20.A0F4V"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F17.A07MX"^^xsd:string ] ;
                    kmn:label "côte de porc"^^xsd:string ;
                    kmn:originalCode "791"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A01RG"^^xsd:string ] ] ;
            kmn:quantity 39.950001 ] ;
    kmn:mealType "Dinner"^^xsd:string .

kmn:consumption-2_consumer-INCA3-211100201 a kmn:FullDayConsumption ;
    kmn:consumptionNumber 2 ;
    kmn:isComposedOf kmn:meal-Breakfast_consumption-2_consumer-INCA3-211100201,
        kmn:meal-Dinner_consumption-2_consumer-INCA3-211100201,
        kmn:meal-Lunch_consumption-2_consumer-INCA3-211100201 ;
    time:before kmn:consumption-3_consumer-INCA3-211100201 .

kmn:consumption-3_consumer-INCA3-211100201 a kmn:FullDayConsumption ;
    kmn:consumptionNumber 3 ;
    kmn:isComposedOf kmn:meal-Breakfast_consumption-3_consumer-INCA3-211100201,
        kmn:meal-Dinner_consumption-3_consumer-INCA3-211100201,
        kmn:meal-Lunch_consumption-3_consumer-INCA3-211100201 .

kmn:meal-Dinner_consumption-2_consumer-INCA3-211100201 a kmn:Meal ;
    kmn:hasFoodComposition [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A01GG#F10.A166Y$F28.A07HS"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F10.A166Y"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F28.A07HS"^^xsd:string ] ;
                    kmn:label "cerise"^^xsd:string ;
                    kmn:originalCode "129"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A01GG"^^xsd:string ] ] ;
            kmn:quantity 46.440002 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A03DL"^^xsd:string ;
                    kmn:label "eau (du robinet)"^^xsd:string ;
                    kmn:originalCode "1504"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A03DL"^^xsd:string ] ] ;
            kmn:quantity 200.0 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A03ZN#F04.A02QE$F04.A00HR$F04.A025C$F04.A01BQ$F19.A07PR$F22.A07SH$F28.A07KQ"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F04.A00HR"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F19.A07PR"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F04.A025C"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F04.A01BQ"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F22.A07SH"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F28.A07KQ"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F04.A02QE"^^xsd:string ] ;
                    kmn:label "pizza"^^xsd:string ;
                    kmn:originalCode "4349"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A03ZN"^^xsd:string ] ] ;
            kmn:quantity 61.0 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A03YM#F04.A031G$F04.A02NC$F04.A02LT$F04.A02QE$F04.A023A$F22.A07SS"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F04.A031G"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F04.A02QE"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F04.A023A"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F22.A07SS"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F04.A02NC"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F04.A02LT"^^xsd:string ] ;
                    kmn:label "quiche salée (avec oeuf)"^^xsd:string ;
                    kmn:originalCode "4355"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A03YM"^^xsd:string ] ] ;
            kmn:quantity 58.0 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A01DY#F10.A166Y$F20.A07QE"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F10.A166Y"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F20.A07QE"^^xsd:string ] ;
                    kmn:label "raisin noir"^^xsd:string ;
                    kmn:originalCode "2399"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A01DY"^^xsd:string ] ] ;
            kmn:quantity 171.12 ] ;
    kmn:mealType "Dinner"^^xsd:string .

kmn:meal-Dinner_consumption-3_consumer-INCA3-211100201 a kmn:Meal ;
    kmn:hasFoodComposition [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A03AH#F22.A07SS"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F22.A07SS"^^xsd:string ] ;
                    kmn:label "jus de fruits 100% pur jus"^^xsd:string ;
                    kmn:originalCode "3246"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A03AH"^^xsd:string ] ] ;
            kmn:quantity 10.4 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A00KJ"^^xsd:string ;
                    kmn:label "pastèque"^^xsd:string ;
                    kmn:originalCode "171"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A00KJ"^^xsd:string ] ] ;
            kmn:quantity 50.0 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A0ETQ#F26.A07XE$F27.A00GZ$F28.A07GS"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F26.A07XE"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F27.A00GZ"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F28.A07GS"^^xsd:string ] ;
                    kmn:label "ail"^^xsd:string ;
                    kmn:originalCode "89"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A0ETQ"^^xsd:string ] ] ;
            kmn:quantity 3.95 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A02JH#F26.A07XE$F28.A07GS$F28.A07KQ"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F26.A07XE"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F28.A07KQ"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F28.A07GS"^^xsd:string ] ;
                    kmn:label "calamar"^^xsd:string ;
                    kmn:originalCode "1030"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A02JH"^^xsd:string ] ] ;
            kmn:quantity 18.0 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A039C#F07.A072X$F10.A0CQE$F19.A07PH"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F10.A0CQE"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F07.A072X"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F19.A07PH"^^xsd:string ] ;
                    kmn:label "beurre doux (non salé)"^^xsd:string ;
                    kmn:originalCode "1099"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A039C"^^xsd:string ] ] ;
            kmn:quantity 5.0 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A03NG#F04.A03MV$F19.A07PF"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F19.A07PF"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F04.A03MV"^^xsd:string ] ;
                    kmn:label "vin blanc moelleux ou liquoreux"^^xsd:string ;
                    kmn:originalCode "4084"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A03NG"^^xsd:string ] ] ;
            kmn:quantity 89.216667 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A037D#F19.A07PR"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F19.A07PR"^^xsd:string ] ;
                    kmn:label "huile de tournesol"^^xsd:string ;
                    kmn:originalCode "1091"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A037D"^^xsd:string ] ] ;
            kmn:quantity 5.0 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A02FZ#F03.A06HY$F26.A07XE$F28.A07GS$F28.A07KP"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F28.A07GS"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F28.A07KP"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F26.A07XE"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F03.A06HY"^^xsd:string ] ;
                    kmn:label "crevette rose"^^xsd:string ;
                    kmn:originalCode "1047"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A02FZ"^^xsd:string ] ] ;
            kmn:quantity 100.0 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A029F#F03.A06HY$F17.A07MZ$F20.A07QQ$F26.A07XE$F28.A07GS$F28.A07KP"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F28.A07GS"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F03.A06HY"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F17.A07MZ"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F26.A07XE"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F28.A07KP"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F20.A07QQ"^^xsd:string ] ;
                    kmn:label "truite n.s."^^xsd:string ;
                    kmn:originalCode "4052"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A029F"^^xsd:string ] ] ;
            kmn:quantity 100.0 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A037D#F19.A07PR"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F19.A07PR"^^xsd:string ] ;
                    kmn:label "huile de tournesol"^^xsd:string ;
                    kmn:originalCode "1091"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A037D"^^xsd:string ] ] ;
            kmn:quantity 0.90000004 ] ;
    kmn:mealType "Dinner"^^xsd:string .

kmn:meal-Lunch_consumption-2_consumer-INCA3-211100201 a kmn:Meal ;
    kmn:hasFoodComposition [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A03DL"^^xsd:string ;
                    kmn:label "eau (du robinet)"^^xsd:string ;
                    kmn:originalCode "1504"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A03DL"^^xsd:string ] ] ;
            kmn:quantity 200.0 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A03KH#F10.A077K$F22.A07SH$F26.A07XE$F28.A07MR"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F26.A07XE"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F28.A07MR"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F22.A07SH"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F10.A077K"^^xsd:string ] ;
                    kmn:label "cappuccino avec caféine (classique)"^^xsd:string ;
                    kmn:originalCode "1490"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A03KH"^^xsd:string ] ] ;
            kmn:quantity 260.0 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A01GM#F10.A0F5H$F10.A166Y$F20.A07QE$F28.A07HS"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F20.A07QE"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F10.A0F5H"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F10.A166Y"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F28.A07HS"^^xsd:string ] ;
                    kmn:label "pêche jaune"^^xsd:string ;
                    kmn:originalCode "3564"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A01GM"^^xsd:string ] ] ;
            kmn:quantity 124.6 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A0BZ9#F03.A06JL$F04.A00QH$F04.A00HR$F04.A00JA$F04.A00RY$F04.A00QJ$F04.A049S$F22.A07SS"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F04.A00JA"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F22.A07SS"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F04.A00QH"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F03.A06JL"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F04.A049S"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F04.A00QJ"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F04.A00HR"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F04.A00RY"^^xsd:string ] ;
                    kmn:label "soupe (de légumes)"^^xsd:string ;
                    kmn:originalCode "3878"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A0BZ9"^^xsd:string ] ] ;
            kmn:quantity 740.66663 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A004Y#F22.A07SH$F24.A07VP"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F24.A07VP"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F22.A07SH"^^xsd:string ] ;
                    kmn:label "pain blanc"^^xsd:string ;
                    kmn:originalCode "520"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A004Y"^^xsd:string ] ] ;
            kmn:quantity 44.099998 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A02NC#F03.A06JK$F10.A077A$F19.A07PR$F28.A07KP"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F03.A06JK"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F10.A077A"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F19.A07PR"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F28.A07KP"^^xsd:string ] ;
                    kmn:label "crème fraîche semi épaisse (semi liquide)"^^xsd:string ;
                    kmn:originalCode "3000"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A02NC"^^xsd:string ] ] ;
            kmn:quantity 4.9000001 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A16EM#F03.A06JA$F10.A0CJK$F20.A07QQ$F24.A07VP"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F10.A0CJK"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F24.A07VP"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F20.A07QQ"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F03.A06JA"^^xsd:string ] ;
                    kmn:label "maquereau"^^xsd:string ;
                    kmn:originalCode "1005"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A16EM"^^xsd:string ] ] ;
            kmn:quantity 135.0 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A046P#F03.A06JH"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F03.A06JH"^^xsd:string ] ;
                    kmn:label "aspartame"^^xsd:string ;
                    kmn:originalCode "1851"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A046P"^^xsd:string ] ] ;
            kmn:quantity 0.059999999 ] ;
    kmn:mealType "Lunch"^^xsd:string ;
    time:before kmn:meal-Dinner_consumption-2_consumer-INCA3-211100201 .

kmn:meal-Lunch_consumption-3_consumer-INCA3-211100201 a kmn:Meal ;
    kmn:hasFoodComposition [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A00JM#F20.A07QE$F28.A07HS"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F28.A07HS"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F20.A07QE"^^xsd:string ] ;
                    kmn:label "concombre"^^xsd:string ;
                    kmn:originalCode "33"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A00JM"^^xsd:string ] ] ;
            kmn:quantity 61.75 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A01DJ#F10.A166Y$F20.A07QE$F28.A07HS"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F28.A07HS"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F10.A166Y"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F20.A07QE"^^xsd:string ] ;
                    kmn:label "pomme pink lady"^^xsd:string ;
                    kmn:originalCode "3629"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A01DJ"^^xsd:string ] ] ;
            kmn:quantity 91.669998 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A02NS#F04.A01EA$F10.A077A$F10.A077J$F19.A07PR"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F10.A077J"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F19.A07PR"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F10.A077A"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F04.A01EA"^^xsd:string ] ;
                    kmn:label "lait fermenté à boire type actimel"^^xsd:string ;
                    kmn:originalCode "2401"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A02NS"^^xsd:string ] ] ;
            kmn:quantity 100.0 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A02RC#F10.A077A$F10.A0CJK$F19.A07PR$F28.A07KP"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F19.A07PR"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F10.A0CJK"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F10.A077A"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F28.A07KP"^^xsd:string ] ;
                    kmn:label "féta"^^xsd:string ;
                    kmn:originalCode "354"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A02RC"^^xsd:string ] ] ;
            kmn:quantity 20.0 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A03KB#F10.A077J$F22.A07SH$F26.A07XE$F28.A07MR"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F28.A07MR"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F10.A077J"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F26.A07XE"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F22.A07SH"^^xsd:string ] ;
                    kmn:label "café court avec caféine (classique)"^^xsd:string ;
                    kmn:originalCode "1478"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A03KB"^^xsd:string ] ] ;
            kmn:quantity 82.5 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A03DT#F19.A07PR"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F19.A07PR"^^xsd:string ] ;
                    kmn:label "eau embouteillée n.s."^^xsd:string ;
                    kmn:originalCode "4275"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A03DT"^^xsd:string ] ] ;
            kmn:quantity 862.5 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A0DMX#F10.A166Y$F20.A07QE$F28.A07HS"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F28.A07HS"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F20.A07QE"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F10.A166Y"^^xsd:string ] ;
                    kmn:label "tomate rouge"^^xsd:string ;
                    kmn:originalCode "45"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A0DMX"^^xsd:string ] ] ;
            kmn:quantity 97.849998 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A00KR#F26.A07XE$F28.A07HS$F28.A07KP"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F28.A07HS"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F26.A07XE"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F28.A07KP"^^xsd:string ] ;
                    kmn:label "salade verte n.s."^^xsd:string ;
                    kmn:originalCode "14"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A00KR"^^xsd:string ] ] ;
            kmn:quantity 55.0 ],
        [ a kmn:FoodComposition ;
            kmn:hasFood [ a kmn:Food ;
                    kmn:foodex2Code "A022T#F10.A077C$F10.A0CJK$F19.A07PR$F20.A0F4V"^^xsd:string ;
                    kmn:hasFacet [ a foodex:Facet ;
                            kmn:code "F10.A077C"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F10.A0CJK"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F20.A0F4V"^^xsd:string ],
                        [ a foodex:Facet ;
                            kmn:code "F19.A07PR"^^xsd:string ] ;
                    kmn:label "jambon cuit sans couenne"^^xsd:string ;
                    kmn:originalCode "4215"^^xsd:string ;
                    foodex:hasBaseTerm [ a foodex:BaseTerm ;
                            kmn:code "A022T"^^xsd:string ] ] ;
            kmn:quantity 22.5 ] ;
    kmn:mealType "Lunch"^^xsd:string ;
    time:before kmn:meal-Dinner_consumption-3_consumer-INCA3-211100201 .

