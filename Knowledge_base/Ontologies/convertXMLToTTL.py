import requests
import json
import os

# Définir le chemin vers le fichier XML
current_dir = os.path.dirname(__file__)
fichier_xml = os.path.join(current_dir, "ontology.drawio")

files = {
    'data': open(fichier_xml, 'rb'),
}

# Requête POST vers l'API
url = "https://chowlk.linkeddata.es/api"

response = requests.post(url, files=files)

# Vérifier le code de statut
if response.status_code == 200:
    # Analyser la réponse JSON
    data = json.loads(response.content)

    # Extraire la clé ttl_data
    ttl_data = data["ttl_data"]
    error = data["errors"]
    warning = data["warnings"]

    print(f"errors : {error}")
    print(f"warnings : {warning}")
    # Enregistrer le contenu dans un fichier
    output_file = os.path.join(current_dir, "ontology.ttl")
    with open(output_file, "w") as f:
        f.write(ttl_data)
        print("**Succès de l'appel**")
else:
    print(f"**Échec** : Erreur lors de l'appel à l'API ({response.status_code})")