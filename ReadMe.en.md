# knowledge-mining-nutrition

## Main stages of building the knowledge base

### Sources

- INCA 2: nomenclature, recipes, individuals
- CIQUAL 2020: nutrients for each ingredient
- Open Food Facts ontology: allergies and the ingredients that cause each allergy
- Wikipedia[^1]: for vegetarian diets

### Integration

Construction of the overall diagram of the knowledge base which brings together: foods, ingredients, nutrients and consumption in the same ontology

### <span style="color:red">Disadvantage 1: INCA - CIQUAL join</span>

The INCA 2 ingredients are based on the CIQUAL 2008 table which cannot be found on the internet 😓 we only have CIQUAL 2020 online.

To find INCA 2 and CIQUAL 2020 ingredients, we used the CIQUAL 2020 search engine.

For each INCA 2 ingredient we launch a POST search request to the CIQUAL website, which gives us the top 5 matches of CIQUAL ingredients for this INCA 2 ingredient.

We then construct a CSV table of INCA 2 ingredients with their top 5 CIQUAL matches as follows:

| incan ingredient 2 | match 1 | match 2 | match 3 | match 4 | match 5 |
| ------------------ | ------- | ------- | ------- | ------- | ------- |
| ...                | ...     | ...     | ...     | ...     | ...     |

The matches are of fairly good quality, most of the ingredients are found in the first match, for the rest a little manual checking quickly gives us the right match (among the remaining matches 2, 3, 4, 5)

It should be noted that there are only ~580 ingredients that we are actually interested in. Although the INCA 2 ingredient list contains more than 800 ingredients, the last part only contains nutrients that are not used in recipes.

### <span style="color:red">Disadvantage 2: Allergenic ingredients</span>

The DGCCRF (Directorate General for Competition, Consumption and Fraud Control) has a list of food allergens on its website[^2].

This list covers the main food allergies, and is exactly the same list proposed in the Open Food Facts ontology[^3].

The advantage of this list is that it brings together (almost) all the allergens for each allergy, and in several languages ​​including French (great!).

Very mechanically, we check for each ingredient whether its wording contains one of the allergens. If so, then we consider it to be an allergen too.

E.g. `egg` is an allergen therefore an ingredient `egg white` is also an allergen.

After assigning each ingredient the allergies it may cause, we performed a small manual check to ensure the validity of the information.

### <span style="color:red">Disadvantage 3: The food origin of each ingredient</span>

We were mainly interested in the following 4 profiles:

- ovo-lacto-vegetarian
- lacto-vegetarian
- ovo-vegetarian
- vegan/vegan

These 4 profiles concern the following 4 origins:

- Animal flesh
- Milk
- Eggs
- Honey

As for animal flesh and honey, it was enough to carry out a manual check to extract the few ingredients that are derived from these origins.

For milk and eggs, we simply repeated the allergy assignment already made; Foods causing a milk allergy will naturally contain milk.

## The recommendations

The recommendations are defined as a multi-dimensional table, the first dimension represents days, the second the meals and the third the foods.

The recommendation system generates recommendations in this format and waits for validation or not from the rules system.

The rules relate to the profiles attached to the consumer concerned by the recommendation.

For example, a consumer who has a milk allergy profile cannot receive a recommendation that contains a dairy product.

## Validation of recommendations

Given a recommendation, it is verified by following the following steps:

1. Insert the recommendation into the knowledge graph.
2. Apply SHACL validators on the resulting graph.
3. Format the output message so that it is easily usable by the recommendation system.

SHACL shapes are defined for each type of constraint, and they are applied to a particular recommendation.

For the moment, the application uses the following shapes:

### A shape to check allergic profiles.

This shape ensures that for any recommended food, it does not contain an ingredient that causes an allergy from which the targeted consumer suffers.

### A shape to check vegetarian profiles.

This shape ensures that for any recommended food, it does not contain an ingredient that is from an origin that the consumer is not authorized to consume.

### A shape to check the association rules.

The association rules are defined in advance by the expert or as a result of observation. To simplify the expert's task, we have defined a standard format for writing association rules:

```math
\begin{gather}
    ( f_{1} \in Meal \lor .. \lor f_{i} \in Meal \lor .. \lor f_{n} \in Meal \implies f'_{1} \in Meal \lor .. \ lor f'_{i} \in Meal \lor .. \lor f'_{m} \in Meal ) \equiv \nonumber \\
    (\neg ( f_{1} \notin Meal \lor .. \lor f_{i} \notin Meal \lor ... \lor f_{n} \notin Meal ) \lor f'_{1} \in Meal \lor .. \lor f'_{i} \in Meal \lor .. \lor f'_{m} \in Meal) \nonumber
\end{gather}
```

where $f_{i}$ are foods in a $Meal$.

The foods in the premise or in the conclusion can be referenced either by their codes, or in group with the code of their category, e.g.:

```python
{
    "if": [
        ("category", 6),
    ],
    "then": [
        ("category", 1),
        ("food", 7740),
        ("food", 7601),
        ("food", 7730),
        ("food", 7741),
        ("food", 7620),
        ("food", 24630),
        ("food", 7710),
    ]
}
```

Once the list of rules is defined, simply follow the notebook code `rules.ipynb` to generate the corresponding SHACL shapes.

### A shape to check the exclusion rules.

The exclusion rules are defined in advance by the expert or as a result of observation. To simplify the expert's task, we have defined a standard format for writing the exclusion rules:

```math
\begin{gather}
    ( f_{1} \in Meal \lor .. \lor f_{i} \in Meal \lor .. \lor f_{n} \in Meal \implies \neg ( f'_{1} \in Meal \lor .. \lor f'_{i} \in Meal \lor .. \lor f'_{m} \in Meal ) ) \equiv \nonumber \\
    (\neg ( f_{1} \notin Meal \lor .. \lor f_{i} \notin Meal \lor ... \lor f_{n} \notin Meal ) \lor \neg ( f'_{1} \in Meal \lor .. \lor f'_{i} \in Meal \lor .. \lor f'_{m} \in Meal ) ) \nonumber
\end{gather}
```

where $f_{i}$ are foods in a $Meal$.

Same as for the association rules, the foods in the premise or in the conclusion can be referenced either by their codes, or in a group with the code of their category, e.g.:

```python
[
    [
        ("category", 3), # presence of a category 3 food
    ],
    [
        ("category", 7) # implies the exclusion of foods from category 7
    ]
]
```

Once the list of rules is defined, simply follow the notebook code `rules.ipynb` to generate the corresponding SHACL shapes.

### A shape to check cardinality rules.

For cardinality rules, there is no simplified format; SHACL shapes are quite easy to edit:

```ttl
[
    a sh:PropertyShape;
    sh:path ( local:hasFoodComposition local:hasFood );
    sh:qualifiedValueShape[
        sh:path ( local:hasCategory local:code );
        sh:in (3 7)
    ] ;
    sh:qualifiedMinCount 1;
    sh:qualifiedMaxCount 1
]
[
    a sh:PropertyShape;
    sh:path ( local:hasFoodComposition local:hasFood );
    sh:qualifiedValueShape[
        sh:path ( local:hasCategory local:code );
        sh:hasValue 0;
    ] ;
    sh:qualifiedMinCount 0;
    sh:qualifiedMaxCount 2
]
```

The first rule states that categories 3 and 7 mixed must have exactly 1 item in the same meal.

The second rule requires category 0 to have a maximum of 2 items in the same meal.

[^1]: _Synoptic table of different vegetarian practices_, last visit date 06/22/2023, https://fr.wikipedia.org/wiki/V%C3%A9g%C3%A9tarisme#Tableau_synoptique_des_diff%C3%A9rentes_pratiques_v %C3%A9g%C3%A9tarian
[^2]: _DG CCRF, Food allergens_, last visit date 06/22/2023, https://www.economie.gouv.fr/dgccrf/Publications/Vie-pratique/Fiches-pratiques/Allergene-alimentaire
[^3]: _Open Food Facts Github_, last visited date 06/22/2023, https://github.com/openfoodfacts/openfoodfacts-server/blob/main/taxonomies/allergens.txt
