# knowledge-mining-nutrition


## Grandes étapes de la construction de la base de connaissance

### Sources

- INCA 2 : nomenclature, recettes, individus
- CIQUAL 2020 : nutriments pour chaque ingrédients
- Open Food Facts ontology : les allergies et les ingrédients qui causent chaque allergie
- Wikipédia[^1]: pour les régimes végétariens

### Intégration

Construction du schéma global de la base de connaissance qui regroupe : aliments, ingrédients, nutriments et consommation dans une même ontologie

### <span style="color:red">Inconvénient 1 : Jointure INCA - CIQUAL</span>

Les ingrédients INCA 2 sont basés sur la table CIQUAL 2008 qui est introuvable sur internet 😓 nous avons que CIQUAL 2020 en ligne.

Pour pouvoir les ingrédients INCA 2 et CIQUAL 2020, nous avons exploité le moteur de recherche CIQUAL 2020.

Pour chaque ingrédient INCA 2 nous lançons une requête POST de recherche vers le site web de CIQUAL, qui nous retroune les top 5 match des ingrédients CIQUAL pour cette ingrédient INCA 2.

Nous construisons en suite une table CSV des ingrédients INCA 2 avec leurs top 5 match CIQUAL comme suit :

| ingrédient inca 2 | match 1 | match 2 | match 3 | match 4 | match 5 |
|-------------------|---------|---------|---------|---------|---------|
|        ...        |   ...   |   ...   |   ...   |   ...   |   ...   |

Les match sont d'assez bonne qualité, la plupart des ingrédients sont trouvés dès le permier match, pour le reste une petite vérificaiton manuelle nous donne rapidement le bon match (parmi les match 2, 3, 4, 5 restant)

Il faut noter qu'il n'y a que ~ 580 ingrédients qui nous intéressent réellement. Malgré que la liste des ingrédients INCA 2 contient plus de 800 ingrédients, la dernière partie ne contient que des nutriments qui ne sont pas utilisés dans les recettes.

### <span style="color:red">Inconvénient 2 : Les ingrédients allergènes</span>

Le DGCCRF (Direction générale de la concurrence, de la consommation et de la répression des fraudes) ressources sur son site web la liste des allergènes alimentaires[^2].

Cette liste couvre les allergies alimentaires principales, et elle est exactement la même liste proposée dans l'ontologie d'Open Food Facts[^3].

L'avantage avec cette liste c'est qu'elle regroupe (presque) tous les allergènes pour chaque allergie, et ce en plusieurs langues y compris le Français (super !).

Très méchaniquement, nous vérifions pour chaque ingrédient si son libellé contient un des allergènes. Si c'est le cas, alors nous le considérons comme lui aussi allergène.

E.g. `oeuf` est un allergène donc un ingrédient `blanc d'oeuf` est aussi un allergène.

Après avoir attribué à chaque ingrédient les allergies qu'il peut causer, nous avons effectué une petite vérification manuelle pour s'assurer de la validité des informations.

### <span style="color:red">Inconvénient 3 : L'origine alimentaire de chaque ingrédient</span>

Nous nous sommes intéressés principalement par intéressé par les 4 profils suivants:

- ovo-lacto-végétarien
- lacto-végétarien
- ovo-végétarien
- végétalien/végan

Ces 4 profils concernent les 4 origines suivantes:

- Chair animal
- Lait
- Oeufs
- Miel

Pour ce qui est de la chair animale et du miel, il a suffit d'effectuer une vérification manuelle pour extraire les quelques ingrédients qui sont dérivés de ces origines.

Pour le lait et les oeufs, nous avons simplement repris l'affectation des allergies déjà réalisée; les aliments causant une allergie au lait vont naturellement contenir du lait.

## Les recommandations

Les recommandations sont définies comme un tableu multi-dimensionel, la première dimension représente les jours, la deuxième les repas et la troisième les aliments.

Le système de recommendation genère des recommandations sous ce format et attend une validation ou non de la part du système de règles.

Les règles sont relatives aux profils attachés au consommateur concerné par la recommandation.

Par exemple, un consommateur qui a un profil allergique au lait ne peut pas recevoir une recommandation qui contient un produit laitier.

## Validation des recommandation

Étant donné une recommandation, elle est vérifiée en suivant les étapes suivantes :

1. Insérer la recommandation dans le graphe de connaissance.
2. Appliquer les validateurs SHACL sur le graphe résultant.
3. Formater le message de sortie afin qu'il soit facilement exploitable par le système de recommandation.

Les shape SHACL sont définis pour chaque type de contrainte, et elles sont appliquées sur une recommandation en particulier.

Pour le moment, l'application exploite les shapes suivantes :

### Une shape pour vérifier les profils allergiques.

Cette shape s'assure que pour tout aliment recommandé, ce dernier ne contient pas un ingrédient qui cause une allergie dont le consommateur ciblé souffre.

### Une shape pour vérifier les profils végétariens.

Cette shape s'assure que pour tout aliment recommandé, ce dernier ne contient pas un ingrédient qui est d'une origine que le consommateur n'est pas autorisé à consommer.

### Une shape pour vérifier les règles d'association.

Les règles d'association sont définies à l'avance par l'expert ou comme résultat d'observation. Pour simplifier la tâche de l'expert, nous avons défini un format type pour rédiger les règles d'association : 

```math
\begin{gather}
    ( f_{1} \in Meal \lor .. \lor f_{i} \in Meal \lor .. \lor f_{n} \in Meal \implies f'_{1} \in Meal \lor .. \lor f'_{i} \in Meal \lor .. \lor f'_{m} \in Meal ) \equiv \nonumber \\
    (\neg ( f_{1} \notin Meal \lor .. \lor f_{i} \notin Meal \lor ... \lor f_{n} \notin Meal ) \lor f'_{1} \in Meal \lor .. \lor f'_{i} \in Meal \lor .. \lor f'_{m} \in Meal) \nonumber
\end{gather}
```

où $f_{i}$ sont des aliments (food) dans un repas $Meal$.

Les aliments dans la prémisse ou dans la conclusion peuvent être référencés soit par leurs codes, ou bien en groupe avec le code de leur categorie, e.g. :

```python
{
    "if": [
        ("category", 6),
    ],
    "then": [
        ("category", 1),
        ("food", 7740),
        ("food", 7601),
        ("food", 7730),
        ("food", 7741),
        ("food", 7620),
        ("food", 24630),
        ("food", 7710),
    ]
}
```

Une fois la liste de règles définie, il suffit donc de suivre le code du notebook `rules.ipynb` pour générer les shape SHACL correspondantes.

### Une shape pour vérifier les règles d'exclusion.

Les règles d'exclusion sont définies à l'avance par l'expert ou comme résultat d'observation. Pour simplifier la tâche de l'expert, nous avons défini un format type pour rédiger les règles d'exclusion :

```math
\begin{gather}
    ( f_{1} \in Meal \lor .. \lor f_{i} \in Meal \lor .. \lor f_{n} \in Meal \implies \neg ( f'_{1} \in Meal \lor .. \lor f'_{i} \in Meal \lor .. \lor f'_{m} \in Meal ) ) \equiv \nonumber \\
    (\neg ( f_{1} \notin Meal \lor .. \lor f_{i} \notin Meal \lor ... \lor f_{n} \notin Meal ) \lor \neg ( f'_{1} \in Meal \lor .. \lor f'_{i} \in Meal \lor .. \lor f'_{m} \in Meal ) ) \nonumber
\end{gather}
```

où $f_{i}$ sont des aliments (food) dans un repas $Meal$.

Même chose que pour les règles d'association, les aliments dans la prémisse ou dans la conclusion peuvent être référencés soit par leurs codes, ou bien en groupe avec le code de leur categorie, e.g. :

```python
[
    [
        ("category", 3), # présence d'un aliment de la catégorie 3
    ],
    [
        ("category", 7) # implique l'exclusion des aliments de la catégorie 7
    ]
]
```

Une fois la liste de règles définie, il suffit donc de suivre le code du notebook `rules.ipynb` pour générer les shape SHACL correspondantes.

### Une shape pour vérifier les règles de cardinalité.

Pour les règles de cardinalité, il n'y a pas de format simplifié ; les shapes SHACL sont assez faciles à éditer :

```ttl
[
    a sh:PropertyShape ;
    sh:path ( local:hasFoodComposition local:hasFood ) ;
    sh:qualifiedValueShape [
        sh:path ( local:hasCategory local:code ) ;
        sh:in ( 3 7 )
    ] ;
    sh:qualifiedMinCount 1 ;
    sh:qualifiedMaxCount 1
]
[
    a sh:PropertyShape ;
    sh:path ( local:hasFoodComposition local:hasFood ) ;
    sh:qualifiedValueShape [
        sh:path ( local:hasCategory local:code ) ;
        sh:hasValue 0 ;
    ] ;
    sh:qualifiedMinCount 0 ;
    sh:qualifiedMaxCount 2
]
```

La première règle indique que les catégories 3 et 7 mélangées doivent avoir exactement 1 élément dans un même repas.

La deuxième règle oblige la catégorie 0 à avoir au maximum 2 élements dans un même repas.


[^1]: *Tableau synoptique des différentes pratiques végétariennes*, date dernière visite 22/06/2023, https://fr.wikipedia.org/wiki/V%C3%A9g%C3%A9tarisme#Tableau_synoptique_des_diff%C3%A9rentes_pratiques_v%C3%A9g%C3%A9tariennes

[^2]: *DG CCRF, Allergènes alimentaires*, date dernière visite 22/06/2023, https://www.economie.gouv.fr/dgccrf/Publications/Vie-pratique/Fiches-pratiques/Allergene-alimentaire

[^3]: *Github de Open Food Facts*, date dernière visite 22/06/2023, https://github.com/openfoodfacts/openfoodfacts-server/blob/main/taxonomies/allergens.txt